Sinhala Unicode
---------------

Add Sinhala Unicode font style to Sinhala ('si') pages, and CSS class:
.lang-si
.lang-si *


Add to custom tags/class

Method 1.
Add css class .lang-si to your HTML tags.

Method 2.
Add SinhalaUnicode font family to your classes

Example:
div { font-family:SinhalaUnicode; } 